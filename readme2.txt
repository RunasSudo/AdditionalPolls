[size=x-large]Additional Polls v1.3.0[/size]
[hr]
Removes the restriction that prevents you from having more than one poll in a topic.

[size=large]How do I...[/size]
[hr]
[size=large]...add new polls to an existing topic[/size]
[hr]
The add poll button is located in the same place as would normally find it (bottom right of the topic (next to the reply button)), you can use to add as many polls as you like.
