[size=x-large]Additional Polls v1.3.0[/size]
[hr]
Removes the restriction that prevents you from having more than one poll in a topic.

[size=large]How do I...[/size]
[hr]
[size=large]...add new polls to an existing topic[/size]
[hr]
The add poll button is located in the same place as would normally find it (bottom right of the topic (next to the reply button)), you can use to add as many polls as you like.

[size=large]...remove a poll[/size]
[hr]
The remove poll button has been moved to be under the Edit Poll link.

[size=large]...completely remove the package (database changes etc.)[/size]
[hr]
There is a file called uninstall.php located inside the zip file for this package, dropping it in your root forum directory will remove all the additional polls and the column in the database it uses.
